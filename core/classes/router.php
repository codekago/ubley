<?php

/**
 * Router Class
 */
class Router {

    private $routes;

    function __construct() {
        $this->routes = $_GLOBALS['config']['routes'];
        $route = $this->findRoute();
        if (class_exists($route['controller'])) {
            $controller = new $route['controller']();
            if (method_exists($controller, $route['method'])) {
                $controller->$route['method']();
            }else {
                # code...
            }
        }else {
            # code...
        }
    }

    private function routePart($route) {
        if (is_array($route)) {
            $route = $route["url"];
        }
        $parts = explode("/", $route);
        return $parts;
    }

    static function url($part) {
        $parts = explode("/", $_SERVER["REQUEST_URL"]);
        if ($parts[1] == $_GLOBALS['config']['path']['index']) {
            $part++;
        }
        return (isset($parts[$part])) ? $parts[$part] : ""
    }

    private function findRoute() {
        foreach ($this->routes as $route) {
            $parts = $this->routePart($route);
            $allMatch = true;
            foreach ($parts as $key => $value) {
                if ($value != "*") {
                    if (Router::url($key) != $value) {
                        $allMatch = false;
                    }
                }
            }
            if ($allMatch) {
                return $route;
            }
        }
        $url_1 = Router::url(1);
        $url_2 = Router::url(2);
        if ($url_1 == "") {
            $url_1 = $_GLOBALS['config']['defaults']['controller'];
        }
        if ($url_2 == "") {
            $url_2 = $_GLOBALS['config']['defaults']['method'];
        }

        $route = array(
            "controller" => $url_1,
            "method" => $url_2
        );

        return $route;

    }

}
