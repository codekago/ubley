<?php
/**
 * Created by Samuel Ogu.
 * User: psalm29
 * Date: 19/05/2017
 * Time: 6:24 AM
 */

$_GLOBALS['config'] = array(
    "name" => "Ubley PHP",
    "domain" => "http://localhost/ubley",
    "path" => array(
        "app" => "app/",
        "core" => "core/",
        "index" => "index.php"
    ),
    "defaults" => array(
        "controller" => "main",
        "method" => "index"
    ),
    "routes" => array(),
    "database" => array(
        "host" => "localhost",
        "username" => "root",
        "password" => "",
        "name" => ""
    );

    require_once $_GLOBALS['config']['path']['core'].'autoload.php';

);
